---
title: Apprendre (avec) les communs
description: L'école du communs propose des formations à l'univers des bien communs.
---

Hors du communs propose des formations à l'univers des biens communs : découvrir, comprendre, pratiquer la gestion collective de ressources.

### [Découvrir le programme]({{< relref "/programme" >}})


