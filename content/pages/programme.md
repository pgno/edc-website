---
title: 'Se former'
url: "programme"
menu:
  main:
    name: "Se former"
    weight: 2
---

Le programme est composé de **modules distincts**. Ensemble, il créé un **parcours complet** de la découverte de l'univers des communs à leur mise en place active dans votre structure. 

Avant de planifier ue formation, un **temps d'échange** vous sera proposé pour **définir vos besoins**, les modules qui y correspondent, et trouver l'intervenant répertorié le plus au fait de vos problématiques. 

Les formats de ces modules sont conçus pour être animé de la manière **la plus participative possible**. Il est donc toujours possible de les proposer dans des durées, taille de groupe et cadres différents : inter, intra, évènements, séminaires, etc... en y **intégrant des temps "sur-mesure"**.

### Dans tous les cas, [parlons-en]({{< relref "/contact" >}})

## Les modules principaux

### #p1. [Découvrir]({{< relref "modules/decouvrir" >}}) : de la ressource partagée aux communs 
Un scenario ludique pour rentrer dans l'univers des biens communs

### #p2. [Contribuer]({{< relref "modules/contribuer" >}}) : Organiser et valoriser le ttravail différement
Des études de cas pour mieux comprendre la contribution

### #p3. Outiller la contribution : [Le budget contributif]({{< relref "modules/budget_contributif" >}})
Mettre en pratique la valorisation de la contribution

### #p4. [Animer]({{< relref "modules/animer" >}}) et prendre soin d'une communauté contributive
Construire son cadre d'attention collectif

## Les modules spécialisés

### #s1. Intégrer [les communs dans l'entreprise]({{< relref "modules/entreprise" >}})
Apprendre à faire vivre plusieurs modèles au sein d'une organisation

### #s2. Les communs numériques
La spécificité de ces biens non-rivaux : ce qui change et ne change pas. Les outils spécifiques.

## Modules complémentaires

### #c1. Méthodologies et outils collaboratifs 
En préparation... avec entrepreneurs Optéos ?

### #c2. Animer une réunion, s'exprimer, communiquer dans un cercle contributif 
En préparation... avec entrepreneurs Optéos ?

### #c3. Comptabilité, social, fiscal, etc... autour des communs 
En préparation... avec Harmonium ?