---
title: "Animer et prendre soin d'une communauté contributive"
url: "animer"
---

### Construire son cadre d'attention collectif
*1/2 journée*

Oeuvrer autour des communs c'est prendre soin : d'une ressource, de soi, des autres contributeurs, de l'espace social définit par ce commun. Quelle attention particulière faut-il avoir ? Quelle forme prend cette attention.

- __Pré-requis__ : Une connaissance minimum des logiques de communs et des pratiques collaboratives ou la participation au module Découverte
- __Modalité__ : Etudes de cas
- __Durée conseillée__ : 1/2 journée
- __Public__ : Membre d'un collectif, d'une association, collectivité, entreprise qui souhaite découvrir une logique de collaboration et de travail différente à intégrer dans sa structure.

### [Revenir au programme]({{< relref "/programme" >}})