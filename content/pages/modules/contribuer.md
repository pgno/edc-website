---
title: 'Organiser et valoriser le travail différement'
url: "contribuer"
---

### Des études de cas pour mieux comprendre la contribution

Les collectifs actifs dans les communs utilisent la notion de contribution. C'est une notion clé de la pratique des communs. Mais c'est aussi une notion parfois difficile à cerner.

Qu'est-ce que cela signifie ? Où se situe la contribution dans le champ du travail ? Quels changements de posture et d'organisation cela implique-t-il ?

Ce module se base sur un ensemble d'études de cas autour de typologies organisationnelles différentes pour amener le participant à s'approprier la notion de contribution par l'exemple.

- __Pré-requis__ : Une connaissance minimum des logiques de communs et des pratiques collaboratives ou la participation au module Découverte
- __Modalité__ : Etudes de cas
- __Durée conseillée__ : 1/2 journée
- __Public__ : Membre d'un collectif, d'une association, collectivité, entreprise qui souhaite découvrir une logique de collaboration et de travail différente à intégrer dans sa structure.

### [Revenir au programme]({{< relref "/programme" >}})