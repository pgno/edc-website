---
title: "Intégrer les communs dans l'entreprise"
url: "enreprise"
---

### Apprendre à faire vivre plusieurs modèles au sein d'une organisation
*1/2 journée + 1/2 journée sur mesure*

La logique de communs est-elle compatible avec les exigences d'une entreprise ? Qu'est-il possible de mettre en place ?

- __Pré-requis__ : Une connaissance minimum des logiques de communs et des pratiques collaboratives ou la participation au module Découverte
- __Modalité__ : Etudes de cas
- __Durée conseillée__ : 1/2 journée
- __Public__ : Membre d'un collectif, d'une association, collectivité, entreprise qui souhaite découvrir une logique de collaboration et de travail différente à intégrer dans sa structure.

### [Revenir au programme]({{< relref "/programme" >}})