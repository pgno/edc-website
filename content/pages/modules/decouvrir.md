---
title: 'De la ressource partagée aux communs'
url: "modules/decouvrir"
---

### Un scenario ludique pour rentrer dans l'univers des biens communs

Ce module est un module ludique et narratif. A partir d'un scenario simple et d'un format d'animation type "jeu de rôle", vous __rentrerez dans la peau d'un groupe d'usagers__ qui va progressivement découvrir les enjeux, les problématiques, et les différentes étapes du passage d'une ressource au statut de commun.

Ce module n'a pas vocation à constituer une base théorique sur les communs. C'est un temps collectif d'imprégnation et de discussion animé qui permet de comprendre et de s'approprier les concepts et le vocabulaire des communs à travers une situation compréhensible de tous.

- __Pré-requis__ : aucun
- __Modalité__ : Scénario prospectif
- __Durée conseillée__ : 1/2 journée
- __Public__ : Toute personne ou groupe constitué qui souhaite s'initier aux communs de manière pratique

### [Revenir au programme]({{< relref "/programme" >}})
