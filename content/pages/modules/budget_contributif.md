---
title: 'Outiller la contribution'
url: "budget_contributif"
---

### Mettre en pratique la valorisation de la contribution
*1/2 journée + 1/2 journée si mise en oeuvre sur Loot*

Comprendre ce qu'est (et n'est pas) un budget contributif. Mettre en place un budget et l'expérimenter grâce à Loot.

- __Pré-requis__ : Une connaissance minimum des logiques de communs et des pratiques collaboratives ou la participation au module Découverte
- __Modalité__ : Etudes de cas
- __Durée conseillée__ : 1/2 journée
- __Public__ : Membre d'un collectif, d'une association, collectivité, entreprise qui souhaite découvrir une logique de collaboration et de travail différente à intégrer dans sa structure.

### [Revenir au programme]({{< relref "/programme" >}})