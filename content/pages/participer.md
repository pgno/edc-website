---
title: 'Participer'
url: "participer"
menu:
  main:
    name: "Participer"
    weight: 3
---

Le programme de formation a été élaboré sur la base d'une pratique régulière des communs depuis plus d'une dizaine d'année. Ce n'est donc pas un ensemble d'apport théorique, mais plutôt le partage d'une expérience concrète de la création et de la gestion de communs sous forme pédaogique.

Ce programme vise à être partagé, diffusé, animé par une communauté qui contribue à l'enrichir de nouveeaux modules et de nouvelles approches. 

## Accéder au contenu du programme

Le programme a été créé sous licence [CC-BY-SA](http://creativecommons.org/licenses/by-sa/4.0/). Son contenu est donc réutilisable et modifiable pour des utilisations commerciales et non commerciales. 

Mais avant de vous ouvrir le contenu et le matériel pédagogique, nous vous proposons d'abord de discuter un peu. Simplement pour mieux se connaître, vérifiez que cela correspond bien à nos attentes mutuelles, garder le contact et suivre ce que vous en ferez plus tard...

On pourra alors vous accompagner dans la prise en main si vous le souhaitez ou vous proposez de co-animer une session par exemple.

## Participer à l'évolution du programme

Qaund vous aurez le matériel en main, vous serez toujours les bienvenus pour y contribuer. En faisant des retours d'expérience, des propositions de modifications, d'ajouts de modules... Nous utilisons un gestionnaire de version pour permettre de collaborer massivement sur des contenus partagés. Après un petit briefing, sur les méthodologies de travail, vous serez à bord !

## Soutenir le programme et l'écosystème

Tous les soutiens sont les acceptés voir encouragés ! En parler autour de vous, engager une discussion, soutenir financièrement l'évolution du programme... Sachez aussi que pour chaque formation réalisée autour de ce programme est reversé une partie de la recette aux communs et structure porteuses de communs qui lui ont permis d'exister. Par exemple, suivre le module "Budget Contributif", c'est indirectement financé la communauté de développeur qui développe le logiciel open-source Loot qui est utilisé dans ce module.

Dans tous les cas, [parlons-en](contact) !
