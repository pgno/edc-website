---
title: Une offre de formation à la découverte et la pratique des communs
date: 2022-11-20
description: "Les biens communs sont des choses bien particulières. Elles vous sont peu être familière ou totalement inconnue. Bien que étudiées depuis des dizaines d'années d'un point de vue théorique, c'est depuis quelques années qu'elles prennent une forme pratiques, organisationnelles et constructives dans un ensemble de communauté à travers le monde."
---

## Les biens communs ?

Since about that time, war had been literally continuous, though strictly speaking it had not always been the same war. For several months during his childhood there had been confused street fighting in London itself, some of which he remembered vividly.

> At germana illo undique ducis et utque leti apta amictu, ego avibus. Viridis
> Munus est tutos posse sede, et est inquit, iussis. Ibat galeae auras non nomina

Inside the flat a fruity voice was reading out a list of figures which had something to do with the production of pig iron. The voice came from an oblong metal plaque like a dulled mirror which formed part of the surface of the right-hand wall. Winston turned a switch and the voice sank somewhat, though the words were still distinguishable. The instrument (the telescreen, it was called) could be dimmed) but there was no way of shutting it off completely.

## Pourquoi se former ?

He moved over to the window: a smallish, frail figure, the meagerness of his body merely emphasized by the blue overalls which were the uniform of the Party. His hair was very fair, his face naturally sanguine, his skin roughened by coarse soap and blunt razor blades and the cold of the winter that had just ended.

## Qui créé cette offre ?

Inside the flat a fruity voice was reading out a list of figures which had something to do with the production of pig iron. The voice came from an oblong metal plaque like a dulled mirror which formed part of the surface of the right-hand wall. Winston turned a switch and the voice sank somewhat, though the words were still distinguishable. The instrument (the telescreen, it was called) could be dimmed) but there was no way of shutting it off completely.

### [Découvrir le programme]({{< relref "/programme" >}})
### [Participer au programme]({{< relref "/participer" >}})
### [Parlons-en]({{< relref "/contact" >}})
